#!/usr/bin/env bash

until redis-cli -h redisboi; do
  printf '%s\n' "Waiting to connect to redis..."
done

printf '%s\n' "Installing packages"
npm install
printf '%s\n' "Done with install"

printf '%s\n' "Starting main..."
npm start
