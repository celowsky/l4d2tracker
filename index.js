#!/usr/bin/env node

/**
 * @typedef Player
 * @property {string} Player.name
 * @property {Number} Player.score
 * @property {Number} Player.time
 */

const Gamedig = require('gamedig');
const readline = require('readline');
const rl = readline.createInterface({input: process.stdin, output: process.stdout});
const util = require('util');

const getLine = (function () {
    const getLineGen = (async function* () {
        for await (const line of rl) {
            yield line;
        }
    })();
    return async () => ((await getLineGen.next()).value);
})();

/**
 * @returns {Promise<string>}
 */
const askForHost = async () => {
    process.stdout.write("Please enter host IP (with port): ");

    const myLine = getLine();
    console.log(`myLine is ${JSON.stringify(myLine)}`);
    return myLine;
}

/**
 * @returns {Promise<Array<Player>>}
 */
const getPlayers = async () => {
    // const answer = await askForHost();
    const answer = process.env.HOST_IP;
    rl.close();
    const parts = answer.split(':');
    const host = parts[0];
    const port = parts[1];
    console.log(`host is ${host}, port is ${port}`);
    await Gamedig.query({
        type: 'left4dead2',
        host,
        port,
    }).then((response) => {
        console.log(`response is ${JSON.stringify(response)}`);

        return response.players;
    });
}

const main = async () => {
    const players = await getPlayers();
    console.log(`players is ${players}`);
    if (players) {
        setTimeout(main, 15000);
    }
}

(async () => {
    await main();
})();
